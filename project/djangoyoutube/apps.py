
from django.apps import AppConfig

import urllib.request
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


# Create your views here.
class YTHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False  # Para saber si estamos dentro de entry
        self.inContent = False  # Si tenemos contenido que queremos leer
        self.inMediaGroup = False
        self.inAuthor = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.videoId = ""
        self.fotoVideo = ""
        self.nombreCanal = ""
        self.enlaceCanal = ""
        self.fecha = ""
        self.descripcion = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:group':
                self.inMediaGroup = True
            elif name == 'author':
                self.inAuthor = True
            elif name == 'published':
                self.inContent = True
        if self.inMediaGroup:
            if name == 'media:thumbnail':
                self.fotoVideo = attrs.get('url')
            if name == 'media:description':
                self.inContent = True
        if self.inAuthor:
            if name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True

    def endElement(self, name):
        if name == 'entry':
            from .models import VideoSeleccionables
            self.inEntry = False
            try:
                newVideo = VideoSeleccionables.objects.get(videoId=self.videoId)
            except VideoSeleccionables.DoesNotExist:
                newVideo = VideoSeleccionables(nombre=self.title, enlace=self.link, videoId=self.videoId,
                                               fotoVideo=self.fotoVideo, nombreCanal=self.nombreCanal,
                                               enlaceCanal=self.enlaceCanal, fecha=self.fecha,
                                               descripcion=self.descripcion)
                newVideo.save()
        elif name == 'media:group':
            self.inMediaGroup = False

        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.videoId = self.content
                self.content = ""
                self.inContent = False
            elif name == 'name':
                self.nombreCanal = self.content
                self.content = ""
                self.inContent = False
            elif name == 'uri':
                self.enlaceCanal = self.content
                self.content = ""
                self.inContent = False
            elif name == 'published':
                self.fecha = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.descripcion = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars

def CleanDB():
    from .models import VideoSeleccionados, VideoSeleccionables
    VideoSeleccionados.objects.all().delete()
    VideoSeleccionables.objects.all().delete()


class DjangoyoutubeConfig(AppConfig):
    name = 'djangoyoutube'

    def ready(self):
        CleanDB()
        Parser = make_parser()
        Parser.setContentHandler(YTHandler())
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
              + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        Parser.parse(xmlStream)



