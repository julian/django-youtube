from django.db import models


# Create your models here.
class VideoSeleccionables(models.Model):
    nombre = models.CharField(max_length=64)
    enlace = models.CharField(max_length=64)
    videoId = models.CharField(max_length=64, default="")
    fotoVideo = models.CharField(max_length=64, default="")
    nombreCanal = models.CharField(max_length=64, default="")
    enlaceCanal = models.CharField(max_length=64, default="")
    fecha = models.CharField(max_length=64, default="")
    descripcion = models.TextField(blank=False, default="")

    def __str__(self):
        return self.nombre


class VideoSeleccionados(models.Model):
    nombre = models.CharField(max_length=64)
    enlace = models.CharField(max_length=64)
    videoId = models.CharField(max_length=64, default="")
    fotoVideo = models.CharField(max_length=64, default="")
    nombreCanal = models.CharField(max_length=64, default="")
    enlaceCanal = models.CharField(max_length=64, default="")
    fecha = models.CharField(max_length=64, default="")
    descripcion = models.TextField(blank=False, default="")

    def __str__(self):
        return self.nombre
