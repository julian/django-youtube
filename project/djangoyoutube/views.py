from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from .models import VideoSeleccionables, VideoSeleccionados


def CheckAndList (videoSeleccionado):
    try:
        c = VideoSeleccionables.objects.get(nombre=videoSeleccionado)
        c.delete()
        q = VideoSeleccionados(nombre=c, enlace=c.enlace, videoId=c.videoId, fotoVideo=c.fotoVideo,
                               nombreCanal=c.nombreCanal, enlaceCanal=c.enlaceCanal, fecha=c.fecha,
                               descripcion=c.descripcion)
        q.save()
    except VideoSeleccionables.DoesNotExist:
        c = VideoSeleccionados.objects.get(nombre=videoSeleccionado)
        c.delete()
        q = VideoSeleccionables(nombre=c, enlace=c.enlace, videoId=c.videoId, fotoVideo=c.fotoVideo,
                                nombreCanal=c.nombreCanal, enlaceCanal=c.enlaceCanal, fecha=c.fecha,
                                descripcion=c.descripcion)
        q.save()

@csrf_exempt
def index(request):
    if request.method == "GET":
        contentList = VideoSeleccionables.objects.all()
        context = {'contentList': contentList}
        response = 'djangoyoutube/index.html'

        return render(request, response, context)

    if request.method == "POST":
        videoSeleccionado = request.POST['action']

        CheckAndList(videoSeleccionado)

        contentList = VideoSeleccionables.objects.all()
        contentList2 = VideoSeleccionados.objects.all()

        context = {'contentList': contentList,
                   'contentList2': contentList2}
        response = 'djangoyoutube/index.html'

        return render(request, response, context)


@csrf_exempt
def videoContent(request, llave):
    c = VideoSeleccionables.objects.get(videoId=llave)
    context = {'content': c}
    response = 'djangoyoutube/content.html'
    return render(request, response, context)
